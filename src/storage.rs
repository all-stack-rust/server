use crate::http_codes::HttpCodes;

pub trait Storage {
    fn fetch(&self, file_name: &str) -> Result<(&[u8], &str), HttpCodes>;
}