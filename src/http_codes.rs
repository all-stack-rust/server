use heapless::{String, consts::*};
use core::fmt::Write;

pub enum HttpCodes {
    C200,
    C403,
    C404,
    C500,
    C501,
}

type HttpString = String<U300>;

impl HttpCodes {
    fn write(msg: &str) -> HttpString {
        let mut str = HttpString::new();
        write!(str, "HTTP/1.1 {}\r\n\r\n", msg).unwrap();
        return str;
    }

    fn write_with_body(msg: &str, content_len: usize, content_type: &str) -> HttpString {
        let mut str = HttpString::new();
        write!(str, "HTTP/1.1 {}\r\nContent-Type: {}\r\nConnection: keep-alive\r\nContent-Length: {}\r\n\r\n", msg, content_type, content_len).unwrap();
        return str;
    }

    pub fn code_to_header(&self, content_len: usize, content_type: &str) -> HttpString {
        return match self {
            HttpCodes::C200 => {
                HttpCodes::write_with_body("200 Ok", content_len, content_type)
            },
            HttpCodes::C404 => {
                HttpCodes::write("404 Not Found")
            },
            HttpCodes::C403 => {
                HttpCodes::write("403 Forbidden")
            },
            HttpCodes::C500 => {
                HttpCodes::write("500 Internal Server Error")
            },
            HttpCodes::C501 => {
                HttpCodes::write("501 Not Implemented")
            }
        }
    }

    pub fn write_code(&self, output: &mut [u8], content_len: usize, content_type: &str) -> usize {

        let str = self.code_to_header(content_len, content_type);
        let str_b = str.as_bytes();

        if output.len() < str_b.len() {
            panic!("Stacksize too low");
        }

        for i in 0..str_b.len() {
            output[i] = str_b[i];
        }

        return str_b.len();
    }

    pub fn write_500_code(output: &mut [u8]) -> usize {
        let code = HttpCodes::C500;
        return code.write_code(output, 0, "");
    }

    pub fn write_200_code(output: &mut [u8], content_len: usize, content_type: &str) -> usize {
        let code = HttpCodes::C200;
        return code.write_code(output, content_len, content_type);
    }

    pub fn write_501_code(output: &mut [u8]) -> usize {
        let code = HttpCodes::C501;
        return code.write_code(output, 0, "");
    }

    pub fn write_403_code(output: &mut [u8]) -> usize {
        let code = HttpCodes::C403;
        return code.write_code(output, 0, "");
    }
}