#![no_std]
#![feature(const_generics)]
#![allow(incomplete_features)]
#[cfg(test)]
#[macro_use]
extern crate std;

pub mod storage;
pub mod http_codes;
pub mod http_iter;

use crate::storage::Storage;
use crate::http_codes::HttpCodes;

use embedded_websocket as ws;
use ws::{
    HttpHeader, WebSocketReceiveMessageType, WebSocketSendMessageType, WebSocketServer,
    WebSocketState,
};
use crate::http_iter::HttpIter;

pub enum ResponseType<'a> {
    TCP(HttpIter<'a>),
    Package(&'a[u8]),
}

#[derive(Debug)]
pub enum WebServerError {
    WebSocket(ws::Error),
}

impl From<ws::Error> for WebServerError {
    fn from(err: ws::Error) -> WebServerError {
        WebServerError::WebSocket(err)
    }
}

pub type WriteTcpResult<'a> = Result<ResponseType<'a>, &'a[u8]>;
pub type WritePackageResult<'a> = Result<&'a[u8], &'a[u8]>;

pub struct Server<T, const STORAGE_SIZE: usize>
    where T: Storage
{
    ws: WebSocketServer,
    page_store: T,
    output: [u8; STORAGE_SIZE],
}

impl<T, const STORAGE_SIZE: usize> Server<T, STORAGE_SIZE>
    where T: Storage
{
    pub fn new(page_store: T) -> Self {
        Server {
            ws: WebSocketServer::new_server(),
            page_store: page_store,
            output: [0; STORAGE_SIZE],
        }
    }

    pub fn put_tcp(&mut self, tcp_input: &[u8]) -> WriteTcpResult {
        if self.ws.state == WebSocketState::Open {
            // if the tcp stream has already been upgraded to a websocket connection
            return self.web_socket_read(tcp_input);
        } else {
            // assume that the client has sent us an http request. Since we may not read the
            // header all in one go we need to check for HttpHeaderIncomplete and continue reading
            return match ws::read_http_header(tcp_input) {
                Ok(http_header) => {
                    self.respond_to_http_request(http_header)
                }
                _ => {
                    let len = HttpCodes::write_501_code(&mut self.output[..]);
                    Err(&self.output[0..len])
                }
            }
        }
    }

    pub fn put_data(&mut self, data_input: &[u8]) -> WritePackageResult {
        if self.ws.state != WebSocketState::Open {
            let len = HttpCodes::write_501_code(&mut self.output[..]);
            return Err(&self.output[0..len]);
        }

        let len = self.ws.write(
            WebSocketSendMessageType::Binary,
            true,
            data_input,
            &mut self.output,
        ).unwrap();
        Ok(&self.output[0..len])
    }

    fn respond_to_http_request(&mut self, http_header: HttpHeader) -> WriteTcpResult {
        if let Some(websocket_context) = http_header.websocket_context {
            // this is a web socket upgrade request
            match self.ws.server_accept(&websocket_context.sec_websocket_key, None, &mut self.output) {
                Ok(len) => {
                    let iter = HttpIter::new(&self.output[0..len], None);
                    Ok(ResponseType::TCP(iter))
                },
                _ => {
                    let len = HttpCodes::write_403_code(&mut self.output[..]);
                    Err(&self.output[0..len])
                }
            }
        } else {
            match self.page_store.fetch(http_header.path.as_str()) {
                Ok((page, page_type)) => {
                    let len = HttpCodes::write_200_code(&mut self.output[..], page.len(), page_type);
                    let iter = HttpIter::new(&self.output[..len], Some(page));
                    Ok(ResponseType::TCP(iter))
                },
                Err(code) => {
                    let len = code.write_code(&mut self.output[..], 0, "");
                    Err(&self.output[0..len])
                }
            }
        }
    }

    fn web_socket_read(&mut self, tcp_input: &[u8]) -> WriteTcpResult {
        let maybe_ws_result = self.ws.read(&tcp_input, &mut self.output);
        if let Ok(ws_result) = maybe_ws_result {
            return match ws_result.message_type {
                WebSocketReceiveMessageType::Binary => {
                    Ok(ResponseType::Package(&self.output[0..ws_result.len_to]))
                },
                WebSocketReceiveMessageType::CloseMustReply => {
                    let len = self.ws.write(
                        WebSocketSendMessageType::CloseReply,
                        true,
                        tcp_input,
                        &mut self.output,
                    ).unwrap();
                    let iter = HttpIter::new(&self.output[0..len], None);
                    Ok(ResponseType::TCP(iter))
                },
                WebSocketReceiveMessageType::Ping => {
                    let len = self.ws.write(
                        WebSocketSendMessageType::Pong,
                        true,
                        tcp_input,
                        &mut self.output).unwrap();
                    let iter = HttpIter::new(&self.output[0..len], None);
                    Ok(ResponseType::TCP(iter))
                },
                _ => {
                    let len = HttpCodes::write_501_code(&mut self.output[..]);
                    Err(&self.output[0..len])
                }
            }
        } else {
            let len = HttpCodes::write_500_code(&mut self.output[..]);
            Err(&self.output[0..len])
        }
    }
}

#[cfg(test)]
mod tests {

    use communication::{FrameBuffer, Tx};
    use heapless::{Vec as HVec, consts::*};
    use crate::{Server, ResponseType, WriteTcpResult};
    use crate::storage::Storage;
    use crate::http_codes::HttpCodes;
    use std::vec::Vec;
    use std::string::ToString;

    struct MockStorage {
        root: &'static str,
        large_file: [u8; 2500],
    }

    impl MockStorage {
        pub fn new() -> Self {
            MockStorage {
                root: "<!doctype html><html><head><h1>Hello world</h1></head><body></body></html>",
                large_file: [1; 2500],
            }
        }
    }

    impl Storage for MockStorage {
        fn fetch(&self, file_name: &str) -> Result<(&[u8], &str), HttpCodes> {

            match file_name {
                "/" => {
                    return Ok((self.root.as_bytes(), "text/html"));
                },
                "/large" => {
                    return Ok((&self.large_file[..], "text/html"));
                },
                _ => {
                    return Err(HttpCodes::C404);
                }
            }
        }
    }

    fn extract_tcp(data: WriteTcpResult) -> Vec<u8> {
        let rec: Vec<u8> = match data.unwrap() {
            ResponseType::TCP(tcp) => {
                tcp.map(|b| *b).collect()
            },
            ResponseType::Package(_package) => {
                Vec::new()
            }
        };
        return rec;
    }

    #[test]
    fn test_get_root_html() {
        let client_request = "GET / HTTP/1.1\nHost: myserver.example.com\nOrigin: http://example.com\n\n";
        let mut ws: Server<MockStorage, 4000> = Server::new(MockStorage::new());
        let rec: Vec<u8> = extract_tcp(ws.put_tcp(client_request.as_bytes()));
        let check = "HTTP/1.1 200 Ok\r\nContent-Type: text/html\r\nConnection: keep-alive\r\nContent-Length: 74\r\n\r\n<!doctype html><html><head><h1>Hello world</h1></head><body></body></html>".to_string();
        assert_eq!(rec, Vec::from(check.as_bytes()));
    }

    #[test]
    fn test_get_not_found() {
        let client_request = "GET /NOT_VALID HTTP/1.1\nHost: myserver.example.com\nOrigin: http://example.com\n\n";
        let mut ws: Server<MockStorage, 4000> = Server::new(MockStorage::new());
        let expect_err = ws.put_tcp(client_request.as_bytes());
        let rec = Vec::from(expect_err.err().unwrap());
        let html =
            "HTTP/1.1 404 Not Found\r\n\r\n";
        assert_eq!(rec, Vec::from(html.as_bytes()));
    }

    #[test]
    fn test_get_large() {
        let client_request = "GET /large HTTP/1.1\nHost: myserver.example.com\nOrigin: http://example.com\n\n";
        let mut ws: Server<MockStorage, 4000> = Server::new(MockStorage::new());
        let mut prefix: Vec<u8> = "HTTP/1.1 200 Ok\r\nContent-Type: text/html\r\nConnection: keep-alive\r\nContent-Length: 2500\r\n\r\n".as_bytes().to_vec();
        let rec: Vec<u8> = extract_tcp(ws.put_tcp(client_request.as_bytes()));
        prefix.append(&mut Vec::from(&[1; 2500][..]));
        assert_eq!(rec, prefix);
    }

    #[test]
    fn test_open_ws() {
        let mut ws = Server::new(MockStorage::new());
        open_ws(&mut ws);
        let rec: Vec<u8> = extract_tcp(ws.put_tcp(&[9, 0]));
        assert_eq!(rec, Vec::from(&[138, 2, 9, 0][..]));
    }

    #[test]
    fn test_send_data() {
        let mut ws = Server::new(MockStorage::new());
        open_ws(&mut ws);
        let mut data: HVec<f32, U100> = HVec::new();
        data.push(1.0).unwrap();
        data.push(10.0).unwrap();
        data.push(5.0).unwrap();
        let original = Tx {
            samples: data,
            sample_time: 1e6,
        };
        let serialized: FrameBuffer = Tx::serialize(&original).unwrap();
        let rec: Vec<u8> = Vec::from(ws.put_data(&serialized).unwrap());
        assert_eq!(rec[2..], serialized[..]);
    }

    fn open_ws(ws: &mut Server<MockStorage, 4000>) {
        let client_request = OPEN_WS_REQUEST;
        let rec: Vec<u8> = extract_tcp(ws.put_tcp(client_request.as_bytes()));
        let responce = "HTTP/1.1 101 Switching Protocols\r\nConnection: Upgrade\r\nUpgrade: websocket\r\nSec-WebSocket-Accept: ptPnPeDOTo6khJlzmLhOZSh2tAY=\r\n\r\n";
        assert_eq!(rec, Vec::from(responce.as_bytes()));
    }

    const OPEN_WS_REQUEST: &str = "GET /chat HTTP/1.1
Host: localhost:5000
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Sec-WebSocket-Version: 13
Origin: http://localhost:5000
Sec-WebSocket-Extensions: permessage-deflate
Sec-WebSocket-Key: Z7OY1UwHOx/nkSz38kfPwg==
Sec-WebSocket-Protocol: chat
DNT: 1
Connection: keep-alive, Upgrade
Pragma: no-cache
Cache-Control: no-cache
Upgrade: websocket

";
}