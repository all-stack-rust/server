use core::iter::Iterator;

#[derive(PartialEq)]
enum HttpIterState {
    HEADER,
    BODY,
    BODYEND,
}

pub struct HttpIter<'a> {
    header: &'a [u8],
    body: Option<&'a [u8]>,
    index: usize,
    state: HttpIterState,
}

impl<'a> HttpIter<'a> {
    pub fn new(header: &'a [u8], body: Option<&'a [u8]>) -> Self {
        HttpIter {
            header,
            body,
            state: HttpIterState::HEADER,
            index: 0,
        }
    }
}

static BODY_END_STR: [u8; 4] = [13, 10, 13, 10];

impl<'a> Iterator for HttpIter<'a> {
    type Item = &'a u8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.state == HttpIterState::HEADER {
            let next = &self.header[self.index];
            self.index += 1;

            if self.index == self.header.len() {
                self.state = HttpIterState::BODY;
                self.index = 0;
            }
            return Some(next);
        } else if self.state == HttpIterState::BODY {
            if let Some(body) = self.body {
                let next = &body[self.index];
                self.index += 1;

                if self.index == body.len() {
                    self.state = HttpIterState::BODYEND;
                    self.index = 0;
                }

                return Some(next);
            } else {
                return None;
            }
        } else {
            if self.index >= 4 {
                return None;
            }

            let b = &BODY_END_STR[self.index];
            self.index += 1;

            return Some(b);
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::http_iter::HttpIter;
    use std::vec::Vec;

    #[test]
    fn test_header() {
        let header = vec![1, 2, 3, 4, 5];
        let iter = HttpIter::new(&header[..], None);
        let check: Vec<u8> = iter.into_iter().map(|b| *b).collect();
        assert_eq!(check, vec![1, 2, 3, 4, 5]);
    }

    #[test]
    fn test_header_and_body() {
        let header = vec![1, 2, 3, 4, 5];
        let body = vec![6, 7, 8, 9, 10];
        let iter = HttpIter::new(&header[..], Some(&body[..]));
        let check: Vec<u8> = iter.into_iter().map(|b| *b).collect();
        assert_eq!(check, vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    }
}